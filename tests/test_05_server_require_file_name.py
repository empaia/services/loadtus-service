import os
from pathlib import Path
from pprint import pprint

import requests
from tusclient import client, exceptions
from tusclient.storage import filestorage

import tests.test_utils as tu


def require_file_name_meta_data(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)

        files_storage_path = Path(f"{tu.TEST_FILES_FOLDER}/store_file_04_1.json")
        files_storage = filestorage.FileStorage(files_storage_path)

        file_name, path, _, _, checksum = tu.create_file(f"{tu.TEST_FILES_FOLDER}/test_04_1.txt", 6400)
        my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=2000,
            metadata={
                "file_name": file_name,
            },
            store_url=True,
            url_storage=files_storage,
        )
        uploader.upload()

        # laodtus
        print("___", f"{tu.LOADTUSSERVICE_DATA_DIR}/{file_name}")
        _, byte_out = container.exec_run(f'bash -c "md5sum {tu.LOADTUSSERVICE_DATA_DIR}/{file_name}"')
        checksum_server = byte_out.split(b" ")[0].decode("utf-8")

        assert checksum_server == checksum
    finally:
        pprint(tu.logs(container))
        container.remove(force=True)
        tu.init_volumes()


def test_require_file_name_meta_data():
    require_file_name_meta_data(tu.ENV_FNAME_TRUE_ID_FALSE_AUTH_FALSE, {})


def test_require_file_name_meta_data_auth_403():
    try:
        require_file_name_meta_data(tu.ENV_FNAME_TRUE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403


def require_file_name_meta_data_file_exists(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)
        for i in range(2):
            file_name, path, _, _, _ = tu.create_file(
                f"{tu.TEST_FILES_FOLDER}/test_04_2_{tu.random_String(10)}.txt", 6400
            )
            if i == 0:
                my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
                _ = my_client.uploader(
                    file_path=path,
                    chunk_size=2000,
                    metadata={
                        "file_name": file_name,
                    },
                )
            else:
                try:
                    _ = my_client.uploader(
                        file_path=path,
                        chunk_size=2000,
                        metadata={
                            "file_name": file_name,
                        },
                    )
                except exceptions.TusCommunicationError as e:
                    # Assert server stopped
                    assert "status 409" in str(e)
    finally:
        container.remove(force=True)
        tu.init_volumes()


def test_require_file_name_meta_data_file_exists():
    require_file_name_meta_data_file_exists(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, {})


def test_require_file_name_meta_data_file_exists_auth_403():
    try:
        require_file_name_meta_data_file_exists(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403


def require_file_name_meta_data_forbidden_char_sequence(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)
        file_name, path, _, _, _ = tu.create_file(f"{tu.TEST_FILES_FOLDER}/test_04_2_{tu.random_String(10)}.txt", 6400)
        try:
            my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
            _ = my_client.uploader(
                file_path=path,
                chunk_size=2000,
                metadata={
                    "file_name": f"../{file_name}",
                },
            )
        except exceptions.TusCommunicationError as e:
            if e.status_code != 422:
                raise e
    finally:
        print(tu.logs(container))
        container.remove(force=True)
        tu.init_volumes()


def test_require_file_name_meta_data_forbidden_char_sequence():
    require_file_name_meta_data_forbidden_char_sequence(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, {})


def test_require_file_name_meta_data_forbidden_char_sequence_auth_403():
    try:
        require_file_name_meta_data_forbidden_char_sequence(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403


def require_file_name_meta_data_sonderzeichen(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)
        files_storage_path = Path(f"{tu.TEST_FILES_FOLDER}/store_file_04_2.json")
        files_storage = filestorage.FileStorage(files_storage_path)

        file_name, path, _, _, checksum = tu.create_file(
            f"{tu.TEST_FILES_FOLDER}/test_04_3_äöüß-_{tu.random_String(10)}.txt", 6400
        )
        my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
        # my_client.set_headers({"metadata-encoding": "latin-1"})
        # ### NOTE: tuspy 1.0.0 finally supports proper utf-8 encoding
        my_client.set_headers({"metadata-encoding": "utf-8"})
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=2000,
            metadata={
                "file_name": file_name,
            },
            store_url=True,
            url_storage=files_storage,
        )
        uploader.upload()

        # loadtus
        print("___", f"{tu.LOADTUSSERVICE_DATA_DIR}/{file_name}")
        _, byte_out = container.exec_run(f'bash -c "md5sum {tu.LOADTUSSERVICE_DATA_DIR}/{file_name}"')
        checksum_server = byte_out.split(b" ")[0].decode("utf-8")
        assert checksum_server == checksum
    finally:
        print(tu.logs(container))
        container.remove(force=True)
        tu.init_volumes()


def test_require_file_name_meta_data_sonderzeichen():
    require_file_name_meta_data_sonderzeichen(tu.ENV_FNAME_TRUE_ID_FALSE_AUTH_FALSE, {})


def test_require_file_name_meta_data_sonderzeichen_auth_403():
    try:
        require_file_name_meta_data_sonderzeichen(tu.ENV_FNAME_TRUE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403


def require_file_name_meta_data_and_delete(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)

        files_storage_path = Path(f"{tu.TEST_FILES_FOLDER}/store_file_04_3.json")
        files_storage = filestorage.FileStorage(files_storage_path)

        file_name, path, _, _, checksum = tu.create_file(
            f"{tu.TEST_FILES_FOLDER}/test_04_3_{tu.random_String(10)}.txt", 6400
        )
        my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=2000,
            metadata={
                "file_name": file_name,
            },
            store_url=True,
            url_storage=files_storage,
        )
        uploader.upload()

        # laodtus
        print("___", f"{tu.LOADTUSSERVICE_DATA_DIR}/{file_name}")
        _, byte_out = container.exec_run(f'bash -c "md5sum {tu.LOADTUSSERVICE_DATA_DIR}/{file_name}"')
        checksum_server = byte_out.split(b" ")[0].decode("utf-8")

        assert checksum_server == checksum

        # delete file
        upload_uid = tu.get_file_uid_from_server_by_checking_info_files(container, file_name)
        url = os.path.join(tu.get_loadtus_files_url(container), upload_uid)
        r = requests.delete(url, headers=headers)
        assert r.status_code == 200

        # check file has been removed
        exit_code, _ = container.exec_run(f'bash -c "test -f {tu.LOADTUSSERVICE_DATA_DIR}/{file_name}"')
        assert exit_code != 0
        r = requests.delete(url, headers=headers)
        assert r.status_code == 404
        assert f"Ressource {upload_uid} does not exist." == r.json()["detail"]

    finally:
        pprint(tu.logs(container))
        container.remove(force=True)
        tu.init_volumes()


def test_require_file_name_meta_data_and_delete():
    require_file_name_meta_data_and_delete(tu.ENV_FNAME_TRUE_ID_FALSE_AUTH_FALSE, {})


def test_require_file_name_meta_data_and_delete_auth_403():
    try:
        require_file_name_meta_data_and_delete(tu.ENV_FNAME_TRUE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403
