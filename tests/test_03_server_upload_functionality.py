import os
import time
from pprint import pprint

import requests
from tusclient import client, exceptions

import tests.test_utils as tu


def create(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)
        tu.wait_for_log(container, "Application startup complete")

        file_name, path, _, _, _ = tu.create_file(f"{tu.TEST_FILES_FOLDER}/test_02_1_{tu.random_String(10)}.txt", 32000)
        my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
        my_client.uploader(
            file_path=path,
            chunk_size=10240,
            metadata={
                # Important for the tests to find the file on the server
                "file_name": file_name
            },
        )
    finally:
        pprint(tu.logs(container))
        container.remove(force=True)
        tu.init_volumes()


def test_create():
    create(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, {})


def test_create_auth_403():
    try:
        create(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403


def create_and_start(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)

        file_name, path, _, _, checksum = tu.create_file(
            f"{tu.TEST_FILES_FOLDER}/test_02_2_{tu.random_String(10)}.txt", 64000
        )
        my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=2000,
            metadata={
                # Important for the tests to find the file on the server
                "file_name": file_name
            },
        )
        uploader.upload()

        # tusd
        upload_uid = tu.get_file_uid_from_server_by_checking_info_files(container, file_name)
        _, byte_out = container.exec_run(f'bash -c "md5sum {tu.LOADTUSSERVICE_DATA_DIR}/{upload_uid}"')
        checksum_server = byte_out.split(b" ")[0].decode("utf-8")

        assert checksum_server == checksum
    finally:
        pprint(tu.logs(container))
        container.remove(force=True)
        tu.init_volumes()
    return upload_uid


def test_create_and_start():
    create_and_start(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, {})


def test_create_and_start_auth_403():
    try:
        create_and_start(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403


def create_start_stop_clientside_and_resume(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)

        file_name, path, _, _, checksum = tu.create_file(
            f"{tu.TEST_FILES_FOLDER}/test_02_3_{tu.random_String(10)}.txt", 64000
        )
        my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=2000,
            metadata={
                # Important for the tests to find the file on the server
                "file_name": file_name
            },
        )
        uploader.upload(stop_at=16000)
        time.sleep(0.5)
        uploader.upload(stop_at=48000)
        time.sleep(0.5)
        uploader.upload()

        # tusd
        upload_uid = tu.get_file_uid_from_server_by_checking_info_files(container, file_name)
        _, byte_out = container.exec_run(f'bash -c "md5sum {tu.LOADTUSSERVICE_DATA_DIR}/{upload_uid}"')
        checksum_server = byte_out.split(b" ")[0].decode("utf-8")

        assert checksum_server == checksum
    finally:
        container.remove(force=True)
        tu.init_volumes()


def test_create_start_stop_clientside_and_resume():
    create_start_stop_clientside_and_resume(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, {})


def test_create_start_stop_clientside_and_resume_auth_403():
    try:
        create_start_stop_clientside_and_resume(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403


def create_and_start_and_delete(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)

        file_name, path, _, _, checksum = tu.create_file(
            f"{tu.TEST_FILES_FOLDER}/test_02_2_{tu.random_String(10)}.txt", 64000
        )
        my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=2000,
            metadata={
                # Important for the tests to find the file on the server
                "file_name": file_name
            },
        )
        uploader.upload()

        # tusd
        upload_uid = tu.get_file_uid_from_server_by_checking_info_files(container, file_name)
        _, byte_out = container.exec_run(f'bash -c "md5sum {tu.LOADTUSSERVICE_DATA_DIR}/{upload_uid}"')
        checksum_server = byte_out.split(b" ")[0].decode("utf-8")

        assert checksum_server == checksum

        # delete file
        url = os.path.join(tu.get_loadtus_files_url(container), upload_uid)
        r = requests.delete(url, headers=headers)
        assert r.status_code == 200

        # check file has been removed
        exit_code, _ = container.exec_run(f'bash -c "test -f {tu.LOADTUSSERVICE_DATA_DIR}/{upload_uid}"')
        assert exit_code != 0
        r = requests.delete(url, headers=headers)
        assert r.status_code == 404
        assert f"Ressource {upload_uid} does not exist." == r.json()["detail"]

    finally:
        pprint(tu.logs(container))
        container.remove(force=True)
        tu.init_volumes()


def test_create_and_start_and_delete():
    create_and_start_and_delete(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, {})


def test_create_and_start_and_delete_auth_403():
    try:
        create_and_start_and_delete(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403
