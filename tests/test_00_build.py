import tests.test_utils as tu


def test_build():
    tu.docker_build()


def start_container(env):
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)
    except Exception:
        pass
    else:
        container.remove(force=True)


# test service build, start auth disabled
def test_start_container():
    start_container(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE)


# test service build, start auth enabled
def test_start_container_auth():
    start_container(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE)
