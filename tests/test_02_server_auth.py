import requests

import tests.test_utils as tu


def auth_test(env, headers, status):
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)

        r = requests.get(tu.get_loadtus_files_url(container).replace("/v1/files", "/v1/auth-test"), headers=headers)
        assert r.status_code == status
    finally:
        container.remove(force=True)


def test_auth_test():
    auth_test(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, {}, 200)


def test_auth_test_auth_403():
    auth_test(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, {}, 403)
