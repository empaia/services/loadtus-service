import requests

import tests.test_utils as tu


def alive(env, status):
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)

        r = requests.get(tu.get_loadtus_files_url(container).replace("/v1/files", "/alive"))
        assert r.status_code == status
        assert r.json()["status"] == "ok"
    finally:
        container.remove(force=True)


def test_alive():
    alive(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, 200)


# alive always responds
def test_alive_auth():
    alive(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, 200)
