import json
import time
from pathlib import Path

from docker.errors import NotFound
from docker.models.containers import Container
from tusclient import client, exceptions
from tusclient.storage import filestorage

import tests.test_utils as tu


def get_checksum(container: Container, path: str) -> str:
    _, byte_out = container.exec_run(f'bash -c "md5sum {path}"')
    checksum = byte_out.split(b" ")[0].decode("utf-8")
    return checksum


def kill_loadtus_service(container: Container):
    container.exec_run('bash -c "killall -s SIGKILL uvicorn ; bash"', detach=True)
    time.sleep(3)


def restart_loadtus_service(container: Container):
    container.exec_run(tu.LOADTUSSERVICE_DOCKER_CMD, detach=True)
    tu.wait_for_log(container, "Application startup complete")


def create_start_stop_serverside_assert_incomplete(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)
        file_size_bytes = 64000

        file_name, path, _, _, checksum = tu.create_file(
            f"{tu.TEST_FILES_FOLDER}/test_03_1_{tu.random_String(10)}.txt", file_size_bytes
        )
        my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=2000,
            metadata={
                # Important for the tests to find the file on the server
                "file_name": file_name
            },
            retries=0,
        )  # RETRIES 0

        stop_at = int(file_size_bytes / 4)
        uploader.upload(stop_at=stop_at)
        time.sleep(0.5)

        # tusd: retrieve some data before killing the container
        upload_uid = tu.get_file_uid_from_server_by_checking_info_files(container, file_name)
        file_path_server = f"{tu.LOADTUSSERVICE_DATA_DIR}/{upload_uid}"
        checksum_server = get_checksum(container, file_path_server)

        info_file_path_server = Path(f"{tu.LOADTUSSERVICE_INFO_DIR}/{upload_uid}.info")
        info_file_content = tu.get_file_content(container, info_file_path_server)
        info_json = json.loads(info_file_content)

        # kill loadtus-service
        container.remove(force=True)

        try:
            uploader.upload()
        except (exceptions.TusUploadFailed, exceptions.TusCommunicationError):
            # Assert server stopped
            pass

        assert checksum_server != checksum
        assert info_json["Size"] == file_size_bytes
        assert info_json["Offset"] == stop_at
    finally:
        try:
            container.remove(force=True)
        except NotFound:
            pass
        finally:
            tu.init_volumes()


def test_create_start_stop_serverside_assert_incomplete():
    create_start_stop_serverside_assert_incomplete(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, {})


def test_create_start_stop_serverside_assert_incomplete_auth_403():
    try:
        create_start_stop_serverside_assert_incomplete(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403


def create_start_stop_serverside_and_resume(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)
        file_size_bytes = 64000

        file_name, path, _, _, checksum = tu.create_file(
            f"{tu.TEST_FILES_FOLDER}/test_03_2_{tu.random_String(10)}.txt", file_size_bytes
        )
        my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=2000,
            metadata={
                # Important for the tests to find the file on the server
                "file_name": file_name
            },
            retries=0,
        )  # RETRIES 0

        stop_at = int(file_size_bytes / 4)
        uploader.upload(stop_at=stop_at)
        time.sleep(0.5)

        # kill loadtus-service
        kill_loadtus_service(container)
        # loadtus-service: restart
        restart_loadtus_service(container)
        # should continue as we have enough nr of retries + retry_delay
        uploader.upload()

        # loadtus-service: retrieve some data
        upload_uid = tu.get_file_uid_from_server_by_checking_info_files(container, file_name)
        file_path_server = f"{tu.LOADTUSSERVICE_DATA_DIR}/{upload_uid}"
        checksum_server = get_checksum(container, file_path_server)

        info_file_path_server = Path(f"{tu.LOADTUSSERVICE_INFO_DIR}/{upload_uid}.info")
        info_file_content = tu.get_file_content(container, info_file_path_server)
        info_json = json.loads(info_file_content)

        assert checksum_server == checksum
        assert info_json["Size"] == file_size_bytes
        assert info_json["Offset"] == file_size_bytes
    finally:
        container.remove(force=True)
        tu.init_volumes()


def test_create_start_stop_serverside_and_resume():
    create_start_stop_serverside_and_resume(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, {})


def test_create_start_stop_serverside_and_resume_auth_403():
    try:
        create_start_stop_serverside_and_resume(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403


def create_start_stop_serverside_and_resume_using_storage_file(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)
        file_size_bytes = 64000

        file_name, path, _, _, checksum = tu.create_file(
            f"{tu.TEST_FILES_FOLDER}/test_03_3_{tu.random_String(10)}.txt", file_size_bytes
        )
        files_storage_path = Path(f"{tu.TEST_FILES_FOLDER}/store_file_02_3.json")
        files_storage = filestorage.FileStorage(files_storage_path)

        for i in range(2):
            my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
            uploader = my_client.uploader(
                file_path=path,
                chunk_size=2000,
                metadata={
                    # Important for the tests to find the file on the server
                    "file_name": file_name
                },
                retries=1,
                retry_delay=5,
                store_url=True,
                url_storage=files_storage,
            )
            if i == 0:
                stop_at = int(file_size_bytes / 4)
                uploader.upload(stop_at=stop_at)
                time.sleep(0.5)
                kill_loadtus_service(container)

                try:
                    uploader.upload()
                except (exceptions.TusUploadFailed, exceptions.TusCommunicationError):
                    # Assert server stopped
                    pass

                # assert something actually got written to files storage
                assert files_storage_path.is_file()
                assert len(files_storage_path.read_text()) > 42

                # restart loadtus-service container
                restart_loadtus_service(container)

            elif i == 1:
                # should continue as we have stored the url in files_storage
                uploader.upload()

        # loadtus-service: retrieve some data
        upload_uid = tu.get_file_uid_from_server_by_checking_info_files(container, file_name)
        file_path_server = f"{tu.LOADTUSSERVICE_DATA_DIR}/{upload_uid}"
        checksum_server = get_checksum(container, file_path_server)

        info_file_path_server = Path(f"{tu.LOADTUSSERVICE_INFO_DIR}/{upload_uid}.info")
        info_file_content = tu.get_file_content(container, info_file_path_server)
        info_json = json.loads(info_file_content)

        assert checksum_server == checksum
        assert info_json["Size"] == file_size_bytes
        assert info_json["Offset"] == file_size_bytes

        # assert the upload did really continue and not start a new one
        _, byte_out = container.exec_run(f'bash -c "ls {tu.LOADTUSSERVICE_DATA_DIR}"')
        out = byte_out.decode("utf-8")
        files = out.split("\n")
        files_cleaned = [f for f in files if f != ""]
        assert len(files_cleaned) == 1
    finally:
        container.remove(force=True)
        tu.init_volumes()


def test_create_start_stop_serverside_and_resume_using_storage_file():
    create_start_stop_serverside_and_resume_using_storage_file(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE, {})


def test_create_start_stop_serverside_and_resume_using_storage_file_auth_403():
    try:
        create_start_stop_serverside_and_resume_using_storage_file(tu.ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403
