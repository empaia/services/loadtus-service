from pathlib import Path

from tusclient import client, exceptions
from tusclient.storage import filestorage

import tests.test_utils as tu


def require_id_and_file_name_meta_data(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)
        files_storage_path = Path(f"{tu.TEST_FILES_FOLDER}/store_file_04_1.json")
        files_storage = filestorage.FileStorage(files_storage_path)

        file_name, path, _, _, checksum = tu.create_file(
            f"{tu.TEST_FILES_FOLDER}/test_04_1_{tu.random_String(10)}.txt", 6400
        )
        my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
        uploader = my_client.uploader(
            file_path=path,
            chunk_size=2000,
            metadata={"file_name": file_name, "id": f"my_fancy_pancy_id_{tu.random_String(5)}"},
            store_url=True,
            url_storage=files_storage,
        )
        uploader.upload()

        # laodtus
        print("___", f"{tu.LOADTUSSERVICE_DATA_DIR}/{file_name}")
        _, byte_out = container.exec_run(f'bash -c "md5sum {tu.LOADTUSSERVICE_DATA_DIR}/{file_name}"')
        checksum_server = byte_out.split(b" ")[0].decode("utf-8")

        assert checksum_server == checksum
    finally:
        container.remove(force=True)
        tu.init_volumes()


def test_require_id_and_file_name_meta_data():
    require_id_and_file_name_meta_data(tu.ENV_FNAME_TRUE_ID_TRUE_AUTH_FALSE, {})


def test_require_id_and_file_name_meta_data_auth_403():
    try:
        require_id_and_file_name_meta_data(tu.ENV_FNAME_TRUE_ID_TRUE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403


def require_id_already_exists(env, headers):
    tu.init_volumes()
    try:
        _, container = tu.build_and_start_loadtus_service(tu.LOADTUSSERVICE_DOCKER_CMD, env)
        files_storage_path = Path(f"{tu.TEST_FILES_FOLDER}/store_file_04_1.json")
        files_storage = filestorage.FileStorage(files_storage_path)

        for _ in [0, 1]:
            try:
                file_name, path, _, _, _ = tu.create_file(
                    f"{tu.TEST_FILES_FOLDER}/test_04_1_{tu.random_String(10)}.txt", 6400
                )
                my_client = client.TusClient(url=tu.get_loadtus_files_url(container), headers=headers)
                uploader = my_client.uploader(
                    file_path=path,
                    chunk_size=2000,
                    metadata={"file_name": file_name, "id": "my_fancy_pancy_id_TWICE"},
                    store_url=True,
                    url_storage=files_storage,
                )
                uploader.upload()
            except exceptions.TusCommunicationError as e:
                if e.status_code != 409:
                    raise e
    finally:
        container.remove(force=True)
        tu.init_volumes()


def test_require_id_already_exists():
    require_id_already_exists(tu.ENV_FNAME_TRUE_ID_TRUE_AUTH_FALSE, {})


def test_require_id_already_exists_auth_403():
    try:
        require_id_already_exists(tu.ENV_FNAME_TRUE_ID_TRUE_AUTH_TRUE, {})
    except exceptions.TusCommunicationError as e:
        assert e.status_code == 403
