from pathlib import Path

from tusclient import client

###################################################
# ## As the filename does not start with "test..." #
# ## pytest will not execute this file             #
# ##                                               #
# ## Intended use:                                 #
# ## - manually start a loadtus-service             #
# ## - run this file [python manual_tests.py]      #
###################################################


def test_big_file():
    # ## Replace with some big file ~2GB approx
    path = Path("./test.mp3")
    # ## file_name must be RELATIVE path
    file_name = "./test.mp3"
    my_client = client.TusClient(url="http://localhost:8000/files")
    uploader = my_client.uploader(
        file_path=str(path), chunk_size=10000000, metadata={"file_name": str(file_name), "encoding": "latin-1"}
    )
    # ## comment to only POST (create) upload but do not start upload
    uploader.upload()


test_big_file()
