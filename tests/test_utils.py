import json
import random
import string
import subprocess
import time
from pathlib import Path
from socket import socket
from typing import List

import docker
from docker.models.containers import Container

################
# paths / urls #
################

LOADTUSSERVICE_MNT = Path("./tests/tmp_server_mnt/")
LOADTUSSERVICE_FILES_MNT = Path(f"{LOADTUSSERVICE_MNT}")
LOADTUSSERVICE_DATA_DIR = Path("/home/appuser/files/")
LOADTUSSERVICE_INFO_DIR = Path("/home/appuser/info_files/")
TEST_FILES_FOLDER = Path("./tests/tmp_client/")

################
# docker stuff #
################

# ENABLE FOR DEBUG PURPOSE ONLY
# (container paths will be mounted and you can inspect uploaded files)
ENABLE_MOUNT = False

LOADTUS_SERVICE_PORT = 8000
LOADTUSSERVICE_DOCKER_IMG_NAME = "loadtus-service"
LOADTUSSERVICE_DOCKER_CMD = (
    f"uvicorn loadtus_service.main:app --port {LOADTUS_SERVICE_PORT} --host 0.0.0.0 --log-level debug"
)
ENV_BASE = [
    "LS_HOST=http://localhost:8000",
    "LS_DATA_DIR=/home/appuser/files/",
    "LS_INFO_DIR=/home/appuser/info_files/",
    "LS_TUS_MAX_SIZE=10240000000",
    "LS_DISABLE_OPENAPI=False",
    "LS_DEBUG=True",
    "LS_ROOT_PATH=",
    "LS_CORS_ALLOW_CREDENTIALS=False",
    'LS_CORS_ALLOW_ORIGINS=["*"]',
]
# No Auth
ENV_FNAME_FALSE_ID_FALSE_AUTH_FALSE = ENV_BASE + [
    "LS_REQUIRE_FILE_NAME_META_DATA=False",
    "LS_REQUIRE_ID_META_DATA=False",
    "LS_API_INTEGRATION=loadtus_service.api.v1.integrations.disable_auth:DisableAuth",
]
ENV_FNAME_TRUE_ID_FALSE_AUTH_FALSE = ENV_BASE + [
    "LS_REQUIRE_FILE_NAME_META_DATA=True",
    "LS_REQUIRE_ID_META_DATA=False",
    "LS_API_INTEGRATION=loadtus_service.api.v1.integrations.disable_auth:DisableAuth",
]
ENV_FNAME_TRUE_ID_TRUE_AUTH_FALSE = ENV_BASE + [
    "LS_REQUIRE_FILE_NAME_META_DATA=True",
    "LS_REQUIRE_ID_META_DATA=True",
    "LS_API_INTEGRATION=loadtus_service.api.v1.integrations.disable_auth:DisableAuth",
]
# Auth
AUTH_ENVS = [
    "LS_IDP_URL=http://localhost:1337/auth/realms/master",
    "LS_AUDIENCE=org.empaia.ci.us",
]
ENV_FNAME_FALSE_ID_FALSE_AUTH_TRUE = (
    ENV_BASE
    + AUTH_ENVS
    + [
        "LS_REQUIRE_FILE_NAME_META_DATA=False",
        "LS_REQUIRE_ID_META_DATA=False",
        "LS_API_INTEGRATION=loadtus_service.api.v1.integrations.empaia:EmpaiaApiIntegration",
    ]
)
ENV_FNAME_TRUE_ID_FALSE_AUTH_TRUE = (
    ENV_BASE
    + AUTH_ENVS
    + [
        "LS_REQUIRE_FILE_NAME_META_DATA=True",
        "LS_REQUIRE_ID_META_DATA=False",
        "LS_API_INTEGRATION=loadtus_service.api.v1.integrations.empaia:EmpaiaApiIntegration",
    ]
)
ENV_FNAME_TRUE_ID_TRUE_AUTH_TRUE = (
    ENV_BASE
    + AUTH_ENVS
    + [
        "LS_REQUIRE_FILE_NAME_META_DATA=True",
        "LS_REQUIRE_ID_META_DATA=True",
        "LS_API_INTEGRATION=loadtus_service.api.v1.integrations.empaia:EmpaiaApiIntegration",
    ]
)


LOADTUSSERVICE_TUS_VOLUMES = {LOADTUSSERVICE_FILES_MNT.absolute(): {"bind": str(LOADTUSSERVICE_DATA_DIR), "mode": "rw"}}


################
# Helper funcs #
################


def free_port():
    """
    port 0 requests a random free port from the OS
    this is a work around, because setting port 0 in docker run does not work as expected
    in very rare cases this work around could result in a race condition
    """
    with socket() as s:
        s.bind(("", 0))
        return s.getsockname()[1]


def get_loadtus_files_url(container):
    return f"http://localhost:{get_port(LOADTUS_SERVICE_PORT, container)}/v1/files"


def get_port(container_port, container):
    return container.ports[f"{container_port}/tcp"][0]["HostPort"]


def docker_build() -> docker.DockerClient:
    client = docker.from_env()
    _, log = client.images.build(path=".", tag=LOADTUSSERVICE_DOCKER_IMG_NAME, network_mode="host")
    for item in log:
        print(item)
    return client


def build_and_start_loadtus_service(cmd: str = LOADTUSSERVICE_DOCKER_CMD, env: List[str] = ENV_BASE):
    client = docker_build()
    volumes = LOADTUSSERVICE_TUS_VOLUMES if ENABLE_MOUNT else None
    port = free_port()
    env[0] = f"LS_HOST=http://localhost:{port}"
    container = client.containers.run(
        image=LOADTUSSERVICE_DOCKER_IMG_NAME,
        command=cmd,
        ports={f"{LOADTUS_SERVICE_PORT}/tcp": port},
        volumes=volumes,
        remove=True,
        detach=True,
        environment=env,
    )
    time.sleep(1)
    wait_for_log(container, "Application startup complete")
    container.reload()
    return client, container


def clear_directory(dir_to_clear: str):
    pth = Path(dir_to_clear)
    for child in pth.iterdir():
        if child.is_file():
            child.unlink()
        else:
            clear_directory(child)
            child.rmdir()


def wait_for_log(container: Container, log_msg: str, timeout: int = 10):
    stdout, stderr = logs(container)
    slept = 0
    print(stdout, stderr)
    while log_msg not in stdout and log_msg not in stderr:
        stdout, stderr = logs(container)
        slept += 0.5
        if slept > timeout:
            raise TimeoutError((stdout, stderr))
        time.sleep(0.5)
        print(stdout, stderr)


def init_volumes():
    LOADTUSSERVICE_FILES_MNT.mkdir(parents=True, exist_ok=True)
    TEST_FILES_FOLDER.mkdir(parents=True, exist_ok=True)
    clear_directory(LOADTUSSERVICE_FILES_MNT)
    clear_directory(TEST_FILES_FOLDER)


def random_String(stringLength: int) -> str:
    return "".join(random.choice(string.ascii_uppercase + string.digits) for _ in range(stringLength))


def create_file(file_name: str, content_len: int, content: str = None):
    path = Path(file_name)
    if content is None:
        content = random_String(content_len)
    path.touch()
    path.write_text(content)
    # wait for big files to be complete before doing anything!
    file_size = -1
    while file_size != path.stat().st_size:
        file_size = path.stat().st_size
        time.sleep(0.1)
    # out = os.system(f'bash -c "md5sum {path}"')
    out = cmdline(f'bash -c "md5sum {path}"')
    checksum = out.split(" ")[0]
    return file_name, path, content_len, content, checksum


def get_file_uid_from_server_by_checking_info_files(container: Container, file_name, time_out: int = 20):
    time_slept = 0
    while True:
        _, byte_out = container.exec_run(f'bash -c "ls {LOADTUSSERVICE_INFO_DIR}"')
        out = byte_out.decode("utf-8")
        files = out.split("\n")
        for f in files:
            if f.endswith(".info"):
                _, byte_out = container.exec_run(f'bash -c "cat {LOADTUSSERVICE_INFO_DIR}/{f}"')
                out = byte_out.decode("utf-8")
                info_json = json.loads(out)
                dest_file_name = info_json["MetaData"]["file_name"]
                if dest_file_name == file_name:
                    return info_json["ID"]
        if time_slept > time_out:
            raise TimeoutError()
        else:
            time.sleep(0.1)
            time_slept += 0.1


def cmdline(command: str) -> str:
    process = subprocess.Popen(args=command, stdout=subprocess.PIPE, shell=True)
    return process.communicate()[0].decode("utf-8")


def get_file_content(container: Container, path: str) -> str:
    _, out = container.exec_run(cmd=f'bash -c "cat {path}"')
    out_str = out.decode("utf-8")
    return out_str


def logs(container):
    stdout = container.logs(stdout=True, stderr=False)
    stderr = container.logs(stdout=False, stderr=True)
    return stdout.decode("utf-8"), stderr.decode("utf-8")
