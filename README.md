# loadtus-service

Server implementing the tus protocol.
See https://tus.io/


## Prerequisite 
1. python installed
2. poetry installed
   * see docs: https://python-poetry.org/docs/
3. docker
4. docker-compose


**ALL COMMANDS MUST BE RUN FROM PROJECT ROOT !**


```bash
python3 -m venv .venv
source .venv/bin/activate
poetry install
export PYTHONPATH=`pwd`
cp sample.env .env
```

## Run on host - Option 1 (dev)

```bash
cp sample.env .env
pip3 install uvicorn
poetry run uvicorn loadtus_service.main:app --reload --port 8000 --host 0.0.0.0 --log-level info
```

## Run via docker-compose - Option 2

```bash
cp sample.env .env
sudo docker-compose up -d --build
```

## Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
* run tests with `pytest`


```bash
poetry shell
```

```bash
black .
isort .
pycodestyle loadtus_service tests
pylint loadtus_service tests
```

### Tests

Tests start stop loadtus-server themselves via python-docker. Service must not be started manually.

run tests:

```bash
poetry run pytest tests -rx --maxfail=1
```

* Note: tests require root rights as they build the image with docker and start the container. Use the following line to run pytest with sudo but preserving your user environment:

```bash
sudo env "PATH=$PATH" "PYTHONPATH=$(pwd):$(pwd)/.venv/lib/python$(python3 -c 'import sys; print(".".join(map(str, sys.version_info[:3])))')/site-packages" pytest
```

## Recommended VSCode Settings

In `.vscode/settings.json`.

```json
{
    "python.linting.pylintEnabled": true,
    "python.linting.pycodestyleEnabled": false,
    "python.formatting.provider": "black"
}
```

### Usage

http://localhost:8000/docs
