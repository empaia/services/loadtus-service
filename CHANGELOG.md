# 0.1.40
* renovate.
  * includes python ^3.8 -> ^3.10
# 0.1.35
* update submodule loadtus_service/api/v1/empaia_receiver_auth
# 0.1.34
* renovate (including pydantic-settings = "^2.0.2", fastapi = "^0.103.0")
* fix settings tus_max_size type to int (was string)
* fix setting api_integration default value
# 0.1.33
* renovate
# 0.1.31 / 32
* update README
* remove pinned tuspy
*refactor tests
# 0.1.30
* renovate
# 0.1.29
* renovate
# 0.1.28
* renovate
# 0.1.27
* renovate
# 0.1.26
* trying to pin tuspy version to 0.2.5 via renovate.json
  * tests fail when updating to 1.0.0
  * temporary solution (tuspy is only used in tests, not the service itself)
  * not sure if syntax in renovate.json is correct, this MUST be checked (see if renovate bot wants to update tuspy in pyproject.toml)
# 0.1.25
* renovate
# 0.1.24
* renovate
# 0.1.23
* renovate
# 0.1.22
* renovate
# 0.1.21
* renovate
# 0.1.20
* renovate configuration
# 0.1.19
* renovate
# 0.1.18
* renovate
# 0.1.17
* renovate
# 0.1.16
* renovate
# 0.1.15
* renovate
# 0.1.14
* renovate
# 0.1.13
* renovate
# 0.1.12
* renovate
# 0.1.11
* updated dependencies
# 0.1.10
* updated dependencies
# 0.1.9
* updated ci
# 0.1.8
* made allow_credentials configurable via cors_allow_credentials
# 0.1.7
* rewritte auth tests to not rely on external running auth service:
  * only test for 403 when auth it activated
# 0.1.6
* fix for 0.1.5
# 0.1.5
* updated receiver-auth
* added setting to rewrite URL in wellknown
# 0.1.4
* added delete route
# 0.1.3
* added empaia-receiver-auth
* added EmpaiaIntegration
* updated tests accordingly

# 0.1.2
* added docker registry versioning

# 0.1.1
* added `/alive` endpoint
