from typing import List

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    data_dir: str = "/loadtus/files/"
    info_dir: str = "/loadtus/info_files/"
    tus_max_size: int = 10240000000
    cors_allow_credentials: bool = False
    cors_allow_origins: List[str] = []
    require_file_name_meta_data: bool = False
    require_id_meta_data: bool = False
    api_integration: str = "loadtus_service.api.v1.integrations.default:Default"
    disable_openapi: bool = False
    debug: bool = False
    root_path: str = ""
    host: str = "http://localhost:8000"

    model_config = SettingsConfigDict(env_prefix="ls_", env_file=".env")
