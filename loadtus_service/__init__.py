from importlib.metadata import version

__version__ = version("loadtus_service")
