import json
from logging import Logger
from typing import Dict

from fastapi import Depends, Request
from fastapi.openapi.models import OAuthFlowClientCredentials, OAuthFlows
from fastapi.security import OAuth2
from pydantic_settings import BaseSettings, SettingsConfigDict

from ..empaia_receiver_auth import Auth


class AuthSettings(BaseSettings):
    idp_url: str = ""
    audience: str = ""
    refresh_interval: int = 300  # seconds
    rewrite_url_in_wellknown: str = ""
    openapi_token_url: str = ""

    model_config = SettingsConfigDict(env_prefix="ls_", env_file=".env")


def make_oauth2_wrapper(auth: Auth, auth_settings: AuthSettings):
    oauth2_scheme = OAuth2(
        flows=OAuthFlows(clientCredentials=OAuthFlowClientCredentials(tokenUrl=auth_settings.openapi_token_url))
    )

    def oauth2_wrapper(request: Request, token=Depends(oauth2_scheme)):
        # checks audience (if no exception, user is allowed to use the service)
        decoded_token = auth.decode_token(token)
        # return decoded token / could be none as loadtus-service doesnt need it
        return decoded_token

    return oauth2_wrapper


class EmpaiaApiIntegration:
    def __init__(self, settings, logger: Logger):
        self.settings = settings
        self.logger = logger

        self.auth_settings = AuthSettings()
        self.logger.info(json.dumps(self.auth_settings.dict(), indent=4))

        self.auth = Auth(
            idp_url=self.auth_settings.idp_url.rstrip("/"),
            refresh_interval=self.auth_settings.refresh_interval,
            audience=self.auth_settings.audience,
            rewrite_url_in_wellknown=self.auth_settings.rewrite_url_in_wellknown,
            logger=self.logger,
        )
        self.oauth2_wrapper = make_oauth2_wrapper(auth=self.auth, auth_settings=self.auth_settings)

    def post_depends(self):
        return Depends(self.oauth2_wrapper)

    def post_hook(self, payload, meta_data: Dict[str, str], base_dir: str, sub_dir: str, filename: str):
        pass

    def get_auth_test_depends(self):
        return Depends(self.oauth2_wrapper)

    def get_auth_test_hook(self, payload):
        pass

    def head_depends(self):
        return Depends(self.oauth2_wrapper)

    def head_hook(self, payload, meta_data: Dict[str, str], base_dir: str, sub_dir: str, filename: str):
        pass

    def patch_depends(self):
        return Depends(self.oauth2_wrapper)

    def patch_hook(self, payload, meta_data: Dict[str, str], base_dir: str, sub_dir: str, filename: str):
        pass

    def options_depends(self):
        return Depends(self.oauth2_wrapper)

    def options_hook(self, payload):
        pass

    def delete_depends(self):
        return Depends(self.oauth2_wrapper)

    def delete_hook(self, payload):
        pass
