import logging
from typing import Dict

from fastapi import Depends, HTTPException, status

from ....settings import Settings


class Default:
    def __init__(self, settings: Settings, logger: logging.Logger = None):
        self.settings = settings

    def post_depends(self):
        return Depends()

    def post_hook(self, payload, meta_data: Dict[str, str], base_dir: str, sub_dir: str, filename: str):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Integration not configured",
        )

    def get_auth_test_depends(self):
        return Depends()

    def get_auth_test_hook(self, payload, meta_data: Dict[str, str], base_dir: str, sub_dir: str, filename: str):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Integration not configured",
        )

    def head_depends(self):
        return Depends()

    def head_hook(self, payload, meta_data: Dict[str, str], base_dir: str, sub_dir: str, filename: str):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Integration not configured",
        )

    def patch_depends(self):
        return Depends()

    def patch_hook(self, payload, meta_data: Dict[str, str], base_dir: str, sub_dir: str, filename: str):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Integration not configured",
        )

    def options_depends(self):
        return Depends()

    def options_hook(self, payload):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Integration not configured",
        )

    def delete_depends(self):
        return Depends()

    def delete_hook(self, payload):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Integration not configured",
        )
