import logging
from typing import Dict

from fastapi import Depends

from ....settings import Settings


def _dummy():
    pass


class DisableAuth:
    def __init__(self, settings: Settings, logger: logging.Logger = None):
        self.settings = settings

    def post_depends(self):
        return Depends(_dummy)

    def post_hook(self, payload, meta_data: Dict[str, str], base_dir: str, sub_dir: str, filename: str):
        pass

    def get_auth_test_depends(self):
        return Depends(_dummy)

    def get_auth_test_hook(self, payload):
        pass

    def head_depends(self):
        return Depends(_dummy)

    def head_hook(self, payload, meta_data: Dict[str, str], base_dir: str, sub_dir: str, filename: str):
        pass

    def patch_depends(self):
        return Depends(_dummy)

    def patch_hook(self, payload, meta_data: Dict[str, str], base_dir: str, sub_dir: str, filename: str):
        pass

    def options_depends(self):
        return Depends(_dummy)

    def options_hook(self, payload):
        pass

    def delete_depends(self):
        return Depends(_dummy)

    def delete_hook(self, payload):
        pass
