import base64
import json
import logging
import uuid
from datetime import datetime
from pathlib import Path
from typing import Dict

from aiofile import AIOFile

from . import constant_strings as cs


class CouldNotRegisterNewUploadException(Exception):
    pass


class NoFileNameMetaSupplied(Exception):
    pass


class FileAlreadyExistsAtGivenPath(Exception):
    pass


class ForbiddenCharSequenceInGivenPath(Exception):
    pass


class IdAlreadyExistsAtGivenPath(Exception):
    pass


async def update_upload(
    info_dir: str,
    base_dir: str,
    sub_dir: str,
    filename: str,
    file_id: str,
    info_file_content: Dict,
    data_bytes: bytes,
    require_file_name_meta_data: bool,
):
    logging.debug("update_upload()_start")
    info_file = Path(f"{info_dir}/{file_id}.info")
    current_offset = info_file_content["Offset"]

    if require_file_name_meta_data:
        data_file = Path(f"{base_dir}/{sub_dir}/{filename}")
    else:
        data_file = Path(f"{base_dir}/{file_id}")

    logging.debug(f'update_upload()_meta_data: {info_file_content["MetaData"]}')
    logging.debug(f"update_upload()_data_file: {data_file}")

    async with AIOFile(data_file, "r+b") as df:
        await df.write(data_bytes, offset=current_offset)
        await df.fsync()

        # only update offset when data_bytes was successfully written
        async with AIOFile(info_file, "w") as f:
            bytes_written = len(data_bytes)
            new_offset = current_offset + bytes_written
            info_file_content["Offset"] = new_offset
            info_file_content["Last-Update"] = str(datetime.now())
            info_str = json.dumps(info_file_content)
            await f.write(info_str)
            await f.fsync()

            # only return new offset when everything was successful
            return new_offset

    # if anything goes wrong, return old offset
    return current_offset


async def register_new_upload(
    info_dir: str,
    base_dir: str,
    sub_dir: str,
    filename: str,
    total_size: int,
    metadata: Dict[str, str],
    require_file_name_meta_data: bool,
    require_id_meta_data: bool,
):
    info_dir = Path(info_dir).absolute()

    if require_id_meta_data:
        uid = metadata["id"]
        if uid in [f.name for f in info_dir.glob("**/*")]:
            raise IdAlreadyExistsAtGivenPath
    else:
        uid = uuid.uuid4().hex
        while uid in [f.name for f in info_dir.glob("**/*")]:
            uid = uuid.uuid4().hex

    if require_file_name_meta_data:
        new_data_file = Path(f"{base_dir}/{sub_dir}/{filename}")
    else:
        new_data_file = Path(f"{base_dir}/{uid}")
    if new_data_file.is_file():
        raise FileAlreadyExistsAtGivenPath()

    new_info_file = Path(f"{info_dir}/{uid}.info")
    new_info_file.touch()
    now = str(datetime.now())
    info_file_content = {
        "ID": uid,
        "Size": total_size,
        "Offset": 0,
        "Created": now,
        "Last-Update": now,
        "MetaData": metadata,
    }

    new_data_file.parent.mkdir(parents=True, exist_ok=True)

    async with AIOFile(new_info_file, "w") as f:
        info_str = json.dumps(info_file_content)
        await f.write(info_str)
        await f.fsync()

        # only create file, when info_file was successfully written
        async with AIOFile(new_data_file, "w") as df:
            await df.write("")
            await df.fsync()
            logging.debug(f"register_new_upload()_touched {new_data_file}")

        logging.debug(f"register_new_upload()_created {new_info_file} " f"with content {info_file_content}")

        # return uid only when everything was successful
        return uid

    raise CouldNotRegisterNewUploadException()


def parse_metadata(meta_data: str, meta_data_encoding: str):
    meta = {}
    # 'metadata-encoding' header should ideally be set.
    # as fallback for other tus clients which do not set the header,
    # this workaround trying [utf-8] (e.g tus-js) and then [latin-1]
    # (e.g. tuspy) will still work.
    if meta_data_encoding is None:
        meta_data_encoding = "utf-8"
    if meta_data:
        for key_value_pair in meta_data.split(","):
            (k, v) = key_value_pair.split()
            try:
                v_decode = base64.b64decode(v.encode("ascii")).decode(meta_data_encoding)
            except UnicodeDecodeError:
                v_decode = base64.b64decode(v.encode("ascii")).decode("latin-1")
            meta[k] = v_decode
    return meta


def get_sub_dir_and_filename_from_metadata(data_dir: str, meta_data: Dict[str, str], require_file_name_meta_data: bool):
    if require_file_name_meta_data:
        # resolve symlinks, normalize and make absolute using resolve
        data_dir = Path(data_dir).resolve()
        rel_file_path = meta_data["file_name"]
        abs_file_path = Path(data_dir, rel_file_path).resolve()
        if data_dir not in abs_file_path.parents:
            raise ForbiddenCharSequenceInGivenPath()
        # get a normalized version of rel_file_path
        norm_rel_file_path = abs_file_path.relative_to(data_dir)
        # split file dir and name
        norm_rel_file_dir = norm_rel_file_path.parent
        file_name = norm_rel_file_path.name
        print(norm_rel_file_dir, file_name)
        return str(norm_rel_file_dir), file_name
    else:
        return ".", None


def get_basic_header(expose_headers: bool = False):
    headers = {"Tus-Resumable": cs.TUS_VERSION(), "Cache-Control": "no-store"}
    if expose_headers:
        headers["Access-Control-Expose-Headers"] = (
            "Location, Upload-Offset, Tus-Version, Tus-Max-Size, Tus-Extension, Upload-Length"
        )
    return headers
