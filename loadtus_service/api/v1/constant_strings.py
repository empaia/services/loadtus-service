def LOADTUSSERVICE_ENV_CONFIG():
    return "LOADTUSSERVICE_CONFIG"


def LOADTUSSERVICECTL_DESCRIPTION():
    return "Command line tool to configure loadtus-service."


def LOADTUSSERVICECTL_INIT_HELP():
    return (
        f"Constructs a string containing configuration params "
        f"to export as environment variable {LOADTUSSERVICE_ENV_CONFIG()}.\n"
    )


def LOADTUSSERVICECTL_INIT_DESCRIPTION():
    return LOADTUSSERVICECTL_INIT_HELP()


def LOADTUSSERVICE_MSG_START(config):
    out = "Starting loadtus-service with the following settings:"
    for attr, value in config.__dict__.items():
        out = f"{out}\n {attr}: {value}"
    return out


def LOADTUSSERVICE_ERROR_ENV_NOT_SET():
    return (
        f"Environment variable {LOADTUSSERVICE_ENV_CONFIG()} not set.\n"
        f"Please start [loadtusserverctl init --help] for further instructions."
    )


def TUS_VERSION():
    return "1.0.0"


def TUS_EXTENSIONS():
    # comma seperated list
    return "creation"
