import os
from ast import literal_eval
from pathlib import Path

from aiofile import AIOFile
from aiofiles.os import remove as async_remove
from fastapi import Header, HTTPException
from fastapi.responses import Response
from starlette.requests import Request

from ...singletons import api_v1_integration, logger, settings
from . import constant_strings as cs
from . import utils

####################################
# Routes                           #
####################################


def add_routes(app):
    @app.options("/v1/files/")
    async def _(
        payload=api_v1_integration.options_depends(),
    ):
        api_v1_integration.options_hook(payload)

        allow_cors = True if len(settings.cors_allow_origins) > 0 else False
        headers = utils.get_basic_header(allow_cors)

        headers["Tus-Version"] = cs.TUS_VERSION()
        headers["Tus-Max-Size"] = str(settings.tus_max_size)
        headers["Tus-Extension"] = cs.TUS_EXTENSIONS()
        return Response(headers=headers, status_code=204)

    @app.post("/v1/files")
    async def _(
        # only if server supports "Creation With Upload" extension
        content_length: int = Header(None),
        # total file size
        upload_length: int = Header(None),
        upload_metadata: str = Header(None),
        tus_resumable: str = Header(...),
        # non-tus headers below.
        metadata_encoding: str = Header(None),
        payload=api_v1_integration.post_depends(),
    ):
        logger.debug(
            f"header IN - POST: "
            f" --- {content_length}"
            f" --- {upload_length}"
            f" --- {upload_metadata}"
            f" --- {tus_resumable}"
            f" --- {metadata_encoding}"
        )

        info_dir = settings.info_dir
        data_dir = settings.data_dir
        require_file_name_meta_data = settings.require_file_name_meta_data
        require_id_meta_data = settings.require_id_meta_data
        meta_data = utils.parse_metadata(upload_metadata, metadata_encoding)
        logger.debug(f"meta_data: {meta_data}")

        allow_cors = True if len(settings.cors_allow_origins) > 0 else False
        headers = utils.get_basic_header(allow_cors)

        if upload_length > int(settings.tus_max_size):
            raise HTTPException(status_code=400, detail="File size exceeds maximum.")
        if require_file_name_meta_data:
            if "file_name" not in meta_data:
                raise HTTPException(
                    status_code=400,
                    detail="""Server running with [require_file_name_meta_data] flag.
                        This requires 'file_name' key in 'Upload-Metadata' Header""",
                )
        if require_id_meta_data:
            if "id" not in meta_data:
                raise HTTPException(
                    status_code=400,
                    detail="""Server running with [require_id_meta_data] flag.
                        This requires 'id' key in 'Upload-Metadata' Header""",
                )

        try:
            sub_dir, filename = utils.get_sub_dir_and_filename_from_metadata(
                data_dir, meta_data, require_file_name_meta_data
            )
            api_v1_integration.post_hook(payload, meta_data, data_dir, sub_dir, filename)
            file_id = await utils.register_new_upload(
                info_dir,
                data_dir,
                sub_dir,
                filename,
                upload_length,
                meta_data,
                require_file_name_meta_data,
                require_id_meta_data,
            )
        except utils.FileAlreadyExistsAtGivenPath as e:
            raise HTTPException(
                status_code=409, detail="File mentioned in header [Upload-Metadata].file_name already exists."
            ) from e
        except utils.IdAlreadyExistsAtGivenPath as e:
            raise HTTPException(status_code=409, detail="Provided Id already exists.") from e
        except utils.ForbiddenCharSequenceInGivenPath as e:
            raise HTTPException(
                status_code=422, detail="Forbidden char sequence in upload Upload-Metadata].file_name detected."
            ) from e

        host = settings.host
        headers["Location"] = f"{host}/v1/files/{file_id}"

        return Response(headers=headers, status_code=201, content=None)

    @app.head("/v1/files/{file_id}")
    async def _(file_id: str, tus_resumable: str = Header(...), payload=api_v1_integration.head_depends()):
        logger.debug(f"header IN - HEAD: --- {tus_resumable}")

        info_dir = settings.info_dir
        data_dir = settings.data_dir
        info_file = Path(f"{info_dir}/{file_id}.info")
        require_file_name_meta_data = settings.require_file_name_meta_data

        if not info_file.is_file():
            raise HTTPException(status_code=404, detail=f"Ressource {file_id} does not exist.")

        allow_cors = True if len(settings.cors_allow_origins) > 0 else False
        headers = utils.get_basic_header(allow_cors)

        async with AIOFile(info_file, "r") as f:
            info_file_content = await f.read()

        info_file_content = literal_eval(info_file_content)
        meta_data = info_file_content["MetaData"]
        sub_dir, filename = utils.get_sub_dir_and_filename_from_metadata(
            data_dir,
            meta_data,
            require_file_name_meta_data,
        )
        api_v1_integration.head_hook(payload, meta_data, data_dir, sub_dir, filename)

        offset_serverside = info_file_content["Offset"]
        upload_length = info_file_content["Size"]
        headers["Upload-Offset"] = str(offset_serverside)
        headers["Upload-Length"] = str(upload_length)
        return Response(headers=headers, status_code=200, content=None)

    @app.patch("/v1/files/{file_id}")
    async def _(
        request: Request,
        file_id: str,
        content_type: str = Header(None),
        content_length: int = Header(None),
        upload_offset: int = Header(None),
        tus_resumable: str = Header(...),
        payload=api_v1_integration.patch_depends(),
    ):
        logger.debug(
            f"header IN - PATCH: "
            f" --- {content_type}"
            f" --- {content_length}"
            f" --- {upload_offset}"
            f" --- {tus_resumable}"
        )

        info_dir = settings.info_dir
        data_dir = settings.data_dir
        require_file_name_meta_data = settings.require_file_name_meta_data
        info_file = Path(f"{info_dir}/{file_id}.info")

        if not info_file.is_file():
            raise HTTPException(status_code=404, detail=f"Ressource {file_id} does not exist.")

        allow_cors = True if len(settings.cors_allow_origins) > 0 else False
        headers = utils.get_basic_header(allow_cors)

        if content_type != "application/offset+octet-stream":
            raise HTTPException(status_code=415, detail="Content type [application/offset+octet-stream] required.")

        async with AIOFile(info_file, "r") as f:
            info_file_content = await f.read()

        info_file_content = literal_eval(info_file_content)
        meta_data = info_file_content["MetaData"]
        sub_dir, filename = utils.get_sub_dir_and_filename_from_metadata(
            data_dir, meta_data, require_file_name_meta_data
        )
        api_v1_integration.patch_hook(payload, meta_data, data_dir, sub_dir, filename)

        offset_serverside = info_file_content["Offset"]
        if offset_serverside != upload_offset:
            raise HTTPException(status_code=409, detail="Invalid Offset.")

        body = b""
        async for chunk in request.stream():
            body += chunk

        new_offset = await utils.update_upload(
            info_dir,
            data_dir,
            sub_dir,
            filename,
            file_id,
            info_file_content,
            body,
            settings.require_file_name_meta_data,
        )

        headers["Upload-Offset"] = str(new_offset)
        return Response(headers=headers, status_code=204, content=None)

    @app.delete("/v1/files/{file_id}")
    async def _(
        file_id: str,
        payload=api_v1_integration.delete_depends(),
    ):
        info_dir = settings.info_dir
        data_dir = settings.data_dir
        require_file_name_meta_data = settings.require_file_name_meta_data
        info_file = Path(f"{info_dir}/{file_id}.info")

        if not info_file.is_file():
            raise HTTPException(status_code=404, detail=f"Ressource {file_id} does not exist.")

        async with AIOFile(info_file, "r") as f:
            info_file_content = await f.read()

        info_file_content = literal_eval(info_file_content)
        meta_data = info_file_content["MetaData"]
        sub_dir, filename = utils.get_sub_dir_and_filename_from_metadata(
            data_dir, meta_data, require_file_name_meta_data
        )

        if filename:
            await async_remove(os.path.join(data_dir, sub_dir, filename))
        else:
            await async_remove(os.path.join(data_dir, file_id))
        await async_remove(info_file)

        api_v1_integration.delete_hook(payload)

        return Response(status_code=200)

    ########################
    # Just for dev purpose #
    ########################

    @app.get("/v1/auth-test")
    async def _(
        payload=api_v1_integration.get_auth_test_depends(),
    ):
        api_v1_integration.get_auth_test_hook(payload)
        return {"Hi": "World"}

    @app.get("/alive")
    async def _():
        data_dir_exists = Path(settings.data_dir).exists()
        info_dir_exists = Path(settings.info_dir).exists()
        if not data_dir_exists or not info_dir_exists:
            return {"status": f"data_dir exists: {data_dir_exists}. info_dir exists: {info_dir_exists}"}
        else:
            return {"status": "ok"}
