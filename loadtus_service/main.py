from pathlib import Path

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import __version__ as version
from .api import add_routes
from .singletons import logger, settings

################################
# Parse Args                   #
################################

logger.info(settings)

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""

################################
# Start server                 #
################################

app = FastAPI(
    debug=settings.debug,
    title="Loadtus Server",
    version=version,
    description="Upload Server implementing the Tus protocol.",
    root_path=settings.root_path,
    openapi_url=openapi_url,
)
add_routes(app)

# Enable Cors depending on config
if len(settings.cors_allow_origins) > 0:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.cors_allow_origins,
        allow_credentials=settings.cors_allow_credentials,
        allow_methods=["*"],
        allow_headers=["*"],
    )

####################################
# Check directorys                 #
####################################

Path(settings.data_dir).expanduser().mkdir(parents=True, exist_ok=True)
Path(settings.info_dir).expanduser().mkdir(parents=True, exist_ok=True)
