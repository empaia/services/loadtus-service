import importlib
from logging import Logger

from .api.v1.integrations.default import Default
from .settings import Settings


def get_api_v1_integration(settings: Settings, logger: Logger):
    if settings.api_integration:
        module_name, class_name = settings.api_integration.split(":")
        module = importlib.import_module(module_name)
        IntegrationClass = getattr(module, class_name)
        return IntegrationClass(settings=settings, logger=logger)

    return Default(settings, logger)
