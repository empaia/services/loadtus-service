from logging import getLogger

from .api_integrations import get_api_v1_integration
from .settings import Settings

logger = getLogger("uvicorn")
settings = Settings()
api_v1_integration = get_api_v1_integration(settings, logger)
