FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.4.2@sha256:5c1a951baeed230dc36a9d36c3689faf7e639a3a2271eb4e64cc7660062d6f85 AS builder
COPY . /loadtus_service
WORKDIR /loadtus_service
RUN mkdir /var/log/loadtus-service
RUN mkdir /loadtus
RUN poetry build && poetry export -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.3.1@sha256:54c07b7d87e7d70248fa590f4ed7e0217e2e11b76bb569724807acc82488197b
COPY --from=builder /loadtus_service/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /loadtus_service/dist /artifacts
ENV SERVICE_NAME loadtus-service
COPY --from=builder --chown=appuser:appuser /var/log/loadtus-service /var/log/$SERVICE_NAME
COPY --from=builder --chown=appuser:appuser /loadtus /loadtus
RUN pip install /artifacts/*.whl
RUN mkdir /home/appuser/files \
&& mkdir /home/appuser/info_files
EXPOSE 8000
CMD uvicorn loadtus_service.main:app --port 8000 --host 0.0.0.0 --log-level debug
